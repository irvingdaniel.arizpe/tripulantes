package com.microservicio.tripulantes.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TripulanteDTO {
	private Long id;
	private String name;
	private String apellidos;
}
