package com.microservicio.tripulantes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.microservicio.tripulantes.entity.Tripulante;
import com.microservicio.tripulantes.exceptions.GeneralServiceException;
import com.microservicio.tripulantes.exceptions.NoDataFountException;
import com.microservicio.tripulantes.exceptions.ValidateServiceException;
import com.microservicio.tripulantes.repository.TripulanteRepository;
import com.microservicio.tripulantes.validators.TripulanteValidator;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TripulanteService {
	
	@Autowired
	private TripulanteRepository tripuRepo;
	
	public List<Tripulante> findAll(Pageable page){
		
		 try {
				List<Tripulante> tripulante = tripuRepo.findAll(page).toList();
				return tripulante;

			} catch ( ValidateServiceException | NoDataFountException  e) {
				log.info(e.getMessage(), e);// error y excepcion
				throw e;//se lanza nuevamente el mensaje con un throw
			} catch (Exception e) {
				// TODO: handle exception
				log.error(e.getMessage(), e);
				throw new GeneralServiceException(e.getMessage(), e);
				//GeneralServiceException(e.getMessage(), e);
			}
		
	}
	
	public Tripulante finById(Long tripulanteId) {
		try {
			log.debug("findById ==>"+tripulanteId);
			Tripulante tripulante = tripuRepo.findById(tripulanteId)
					.orElseThrow(() -> new NoDataFountException("No existe el tripulante"));
			return tripulante;
			
		}catch ( ValidateServiceException | NoDataFountException  e) {
			log.info(e.getMessage(), e);// error y excepcion
			throw e;//se lanza nuevamente el mensaje con un throw
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
			//GeneralServiceException(e.getMessage(), e);
		}
	}
	
	@Transactional
	public Tripulante save (Tripulante tripulante) {
		try {
			
			TripulanteValidator.save(tripulante);
			if(tripulante.getId()==null){
				Tripulante newTripulante = tripuRepo.save(tripulante);
				return newTripulante;
			}
			Tripulante exitTripulante = tripuRepo.findById(tripulante.getId())
					.orElseThrow(() -> new NoDataFountException("No existe el tripulante"));
			exitTripulante.setName(tripulante.getName());
			exitTripulante.setApellidos(tripulante.getApellidos());
			tripuRepo.save(exitTripulante);
			return exitTripulante;
			
			
		}catch ( ValidateServiceException | NoDataFountException  e) {
			log.info(e.getMessage(), e);// error y excepcion
			throw e;//se lanza nuevamente el mensaje con un throw
		} catch (Exception e) {
			// TODO: handle exception
			log.error(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
			//GeneralServiceException(e.getMessage(), e);
		}
	}
	
	
	@Transactional
	public void delete(Long tripulanteId) {
		
		try {
			Tripulante tripulante = tripuRepo.findById(tripulanteId)
					.orElseThrow(() -> new NoDataFountException("No existe el tripulante"));
			tripuRepo.delete(tripulante);
			
		}catch (ValidateServiceException | NoDataFountException e) {
			log.info(e.getMessage(), e);
			throw e;
		}catch (Exception e) {
			log.info(e.getMessage(), e);
			throw new GeneralServiceException(e.getMessage(), e);
		}
		
	}
	

}
