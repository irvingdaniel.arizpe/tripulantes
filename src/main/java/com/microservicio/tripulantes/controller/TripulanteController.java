package com.microservicio.tripulantes.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservicio.tripulantes.converter.TripulanteConverter;
import com.microservicio.tripulantes.dto.TripulanteDTO;
import com.microservicio.tripulantes.entity.Tripulante;
import com.microservicio.tripulantes.services.TripulanteService;
import com.microservicio.tripulantes.utils.WrapperResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class TripulanteController {

	@Autowired
	private TripulanteService tripulanteService;
	
	private TripulanteConverter converter = new TripulanteConverter();
	
	private List<Tripulante> tripulante = new ArrayList<>();

	
	 @GetMapping(value="/Tripulantes")
	 public ResponseEntity<List<TripulanteDTO>> findAll(
		 @RequestParam (value="pageNumber", required = false, defaultValue = "0") int pageNumber,
		 @RequestParam (value="pageZise", required= false, defaultValue = "10") int pageZise
		 ){
		 Pageable page = PageRequest.of(pageNumber, pageZise);
		 List<Tripulante> tripulante = tripulanteService.findAll(page);
		 List<TripulanteDTO> dtotripulante = converter.fromEntity(tripulante);
		 
		 return  new WrapperResponse(true, "succes", dtotripulante)
	     		.createResponse(HttpStatus.OK);
	 }
	
	 @GetMapping(value="/tripulantes/{tripulanteId}")
	 public ResponseEntity<WrapperResponse<TripulanteDTO>> finById(@PathVariable("tripulanteId") Long tripulanteId){
		 Tripulante tripulante = tripulanteService.finById(tripulanteId);
		 TripulanteDTO tripulanteDTO = converter.fromEntity(tripulante);
		 return new WrapperResponse<TripulanteDTO>(true, "succes", tripulanteDTO)
				 .createResponse(HttpStatus.OK);
	 }
	 
	 @PostMapping(value="/tripulantes")
	 public ResponseEntity<TripulanteDTO> create (@RequestBody TripulanteDTO tripulante){
		 Tripulante newTripulante = tripulanteService.save(converter.fromDTO(tripulante));
		 TripulanteDTO tripulanteDTO = converter.fromEntity(newTripulante);
		 
		 return new WrapperResponse(true, "succes", tripulanteDTO)
				 .createResponse(HttpStatus.OK);
		 
	 }
	 
	 
	 @PutMapping(value="/tripulantes")
	 public ResponseEntity<TripulanteDTO> update(@RequestBody TripulanteDTO tripulante){
		 Tripulante updatenave = tripulanteService.save(converter.fromDTO(tripulante));
		 TripulanteDTO tripulanteDTO = converter.fromEntity(updatenave);
		 return new WrapperResponse(true, "succes", tripulanteDTO)
				 .createResponse(HttpStatus.OK);
	 }
	 
	 
	 @DeleteMapping(value="/tripulantes/{tripulanteId}")
	 public ResponseEntity<?> delete(@PathVariable("tripulanteId") Long tripulanteId){
		 tripulanteService.delete(tripulanteId);
		 return new WrapperResponse(true, "succes", null)
				 .createResponse(HttpStatus.OK);
		 
	 }


	
}
