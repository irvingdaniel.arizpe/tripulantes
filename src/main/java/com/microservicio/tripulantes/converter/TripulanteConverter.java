package com.microservicio.tripulantes.converter;

import com.microservicio.tripulantes.dto.TripulanteDTO;
import com.microservicio.tripulantes.entity.Tripulante;

public class TripulanteConverter extends AbstractCoverter<Tripulante, TripulanteDTO> {

	@Override
	public TripulanteDTO fromEntity(Tripulante entity) {
        if(entity==null) return null;
		return TripulanteDTO.builder()
				.id(entity.getId())
				.name(entity.getName())
				.apellidos(entity.getApellidos())
				.build();
	}

	@Override
	public Tripulante fromDTO(TripulanteDTO dto) {
		if(dto == null) return null;
		return Tripulante.builder()
				.id(dto.getId())
				.name(dto.getName())
				.apellidos(dto.getApellidos())
				.build();
		
	}
	
	

}
