package com.microservicio.tripulantes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.NO_CONTENT)
public class NoDataFountException extends RuntimeException {

	public NoDataFountException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NoDataFountException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public NoDataFountException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NoDataFountException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NoDataFountException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
