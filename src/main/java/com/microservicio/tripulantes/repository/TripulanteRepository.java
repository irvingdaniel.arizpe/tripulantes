package com.microservicio.tripulantes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.microservicio.tripulantes.entity.Tripulante;



@Repository
public interface TripulanteRepository extends JpaRepository<Tripulante, Long> {
	

}
