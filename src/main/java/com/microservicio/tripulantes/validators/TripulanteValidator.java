package com.microservicio.tripulantes.validators;

import com.microservicio.tripulantes.entity.Tripulante;
import com.microservicio.tripulantes.exceptions.ValidateServiceException;

public class TripulanteValidator {
	
	public static void save(Tripulante tripulante) {
		
		if(tripulante.getName()==null || tripulante.getName().trim().isEmpty()) {
			throw new ValidateServiceException("El nombre es requerido");
		}
		
		if(tripulante.getName().length()>150) {
			throw new ValidateServiceException("El nombre admite 150 caracteres");
		}
		
		if(tripulante.getApellidos()==null || tripulante.getApellidos().trim().isEmpty()) {
			throw new ValidateServiceException("El apellido es requerido");
		}
		
		if(tripulante.getApellidos().length()>200) {
			throw new ValidateServiceException("no se admite mas caracteres mayor a 150 caracteres");
		}
		
	}

}
